const express = require('express')
const config = require('config')
const { join } = require('path')
const webpack = require('webpack')

const app = express()

const isProduction = (process.env.NODE_ENV === 'production')

if (isProduction === true) {
  console.log('PRODUCTION MODE')

  app.use('/static', express.static(join(__dirname, './static/dist')))
} else {
  console.log('DEBUG MODE')

  const wpconfig = require('./webpack.config')(isProduction)
  const webpackDevMiddleware = require('webpack-dev-middleware')
  const webpackHotMiddleware = require('webpack-hot-middleware')
  const compiler = webpack(wpconfig)
  app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: wpconfig.output.publicPath }))
  app.use(webpackHotMiddleware(compiler))

  app.use('/static', express.static(join(__dirname, './static/')))
}
app.use('/assets', express.static(join(__dirname, './static/assets')))

app.get('*', (req, res) => res.sendFile(join(__dirname, '/static/index.html')))

let port
switch (true) {
  case (process.env.APP_PORT !== undefined):
    port = process.env.APP_PORT
    break
  case (config.has('port')):
    port = config.get('port')
    break
  default:
    console.log('APP_PORT environment variable or config port wasn\'t passed.')
    console.log('exiting...')
    process.exit()
}

app.listen(port, (error) => {
  if (error) {
    console.error(error)
  } else {
    console.info('==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port)
  }
})

