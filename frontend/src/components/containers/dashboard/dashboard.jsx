import { connect } from 'react-redux'

import Header from './header/header'
import styles from './dashboard.module.scss'

const Dashboard = props => (
  <div className={styles.container}>
    <Header />
    { props.fetching === false && (props.children)}
  </div>
)

Dashboard.propTypes = {
  fetching: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired
}

export default connect(
  state => ({
    fetching: state.rootReducer.user.fetching
  })
)(Dashboard)
