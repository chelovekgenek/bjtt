import axios from 'helpers/axios'

const BASE = 'users'

export const login = params => async () => {
  const result = await axios.post(`${BASE}/login`, params)
  return result.data
}
export const loginByToken = () => async () => {
  const result = await axios.get(`${BASE}/me`)
  return result.data
}
export const logout = () => async () => {
  const result = await axios.post(`${BASE}/logout`)
  return result.data
}
export const get = params => async () => {
  const filter = {}
  const result = await axios.get(`${BASE}/${params.id}?filter=${JSON.stringify(filter)}`)
  return result.data
}
