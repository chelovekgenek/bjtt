import { call, put, takeEvery } from 'redux-saga/effects'
import { browserHistory } from 'react-router'

import * as token from 'helpers/token'
import * as c from 'constants/actions'
import * as a from 'actions/user'
import * as r from 'constants/routes'
import * as api from './api/user'

function* handleLogin({ payload }) {
  const { username, password, byToken } = payload
  try {
    let userId = 0
    if (byToken === true) {
      const tkn = token.get()
      if (tkn === null) throw new Error('Storage does not contain a token')
      const result = yield call(api.loginByToken())
      userId = result.userId
    } else {
      const result = yield call(api.login({ username, password }))
      userId = result.userId
      token.set(result.id, true)
    }
    const resultGet = yield call(api.get({ id: userId }))

    yield put(a.successLogin(resultGet))
    browserHistory.replace(r.TASK_LIST)
  } catch (error) {
    yield put(a.errorLogin(error.message))
  }
}
function* handleLogout() {
  try {
    yield call(api.logout())

    yield put(a.successLogout())

    token.clear()
  } catch (error) {
    yield put(a.errorLogout(error.message))
  }
}

export default function* watcherUser() {
  yield takeEvery(c.API_LOGIN_USER_REQUEST, handleLogin)
  yield takeEvery(c.API_LOGOUT_USER_REQUEST, handleLogout)
}
