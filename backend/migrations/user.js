const dummyAdmin = {
  password: '123',
  email: 'admin@dummy.com',
  username: 'admin',
};

let createUser = async (dataSource, data, roleName) => {
  const { email, username, password } = data;
  const user = await dataSource.models.user.create({
    email,
    username,
    password,
  });

  return await user.save();
};

module.exports = {
  up: async (dataSource, next) => {
    await createUser(dataSource, dummyAdmin, 'admin');
    next();
  },
  down: async (dataSource, next) => {
    await dataSource.models.user.destroyAll();
    next();
  },
};
