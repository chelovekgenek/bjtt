import { Panel, Button } from 'react-bootstrap'
import classNames from 'classnames'

import styles from './task-card.module.scss'

const TaskCard = props => (
  <Panel className={classNames({
    [styles.container]: true,
    [styles.preview]: props.preview
  })}
  >
    {props.onEdit !== null && (
      <Button
        className={styles.edit}
        onClick={props.onEdit}
      >Edit</Button>
    )}
    {props.done === true && (<h4 className={styles.done}>Done</h4>)}
    <div className={styles.item}>
      <div>Email:</div>
      <div>{ props.email }</div>
    </div>
    <div className={styles.item}>
      <div>Username:</div>
      <span>{ props.username }</span>
    </div>
    { props.image !== '' && <img src={props.image} className={styles.pic} />}
    <p>{ props.text }</p>
  </Panel>
)

TaskCard.defaultProps = {
  image: '',
  preview: false,
  onEdit: null,
  done: false
}
TaskCard.propTypes = {
  email: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  done: PropTypes.bool,
  image: PropTypes.string,
  preview: PropTypes.bool,
  onEdit: PropTypes.func
}

export default TaskCard
