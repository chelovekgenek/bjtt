import * as c from 'constants/actions'

const initialState = {
  data: [],
  selected: {},
  count: 0,
  skip: 0,
  fetching: false,
  error: false
}

export default function (state = initialState, action) {
  switch (action.type) {
    case c.API_ADD_TASK_REQUEST:
    case c.API_GET_TASK_LIST_REQUEST:
      return { ...state, fetching: true }

    case c.API_ADD_TASK_SUCCESS:
      return { ...state, fetching: false }
    case c.API_GET_TASK_LIST_SUCCESS:
      return {
        ...state,
        fetching: false,
        data: action.payload.data,
        count: action.payload.count,
        skip: action.payload.skip
      }

    case c.API_ADD_TASK_ERROR:
    case c.API_GET_TASK_LIST_ERROR:
      return { ...state, fetching: false, error: true }

    case c.API_LOGOUT_USER_SUCCESS:
      return { ...state, selected: {} }

    case c.SELECT_TASK:
      return { ...state, selected: action.payload }
    case c.DESELECT_TASK:
      return { ...state, selected: {} }

    default:
      return state
  }
}
