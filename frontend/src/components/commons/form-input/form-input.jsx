import {
  FormGroup, FormControl, ControlLabel
} from 'react-bootstrap'

const FormInput = ({ label, error, ...props }) => (
  <FormGroup
    validationState={error === false ? null : 'warning'}
  >
    <ControlLabel>{label}</ControlLabel>
    <FormControl {...props} />
    <FormControl.Feedback />
  </FormGroup>
)
FormInput.defaultProps = {
  error: false
}
FormInput.propTypes = {
  label: PropTypes.string.isRequired,
  error: PropTypes.bool
}

export default FormInput
