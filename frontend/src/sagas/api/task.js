import axios from 'helpers/axios'

const BASE = 'tasks'

export const add = data => async () => {
  const result = await axios.post(`${BASE}`, data)

  return result.data
}
export const edit = (id, data) => async () => {
  const result = await axios.put(`${BASE}/${id}`, data)

  return result.data
}

export const getList = (skip = 0, order = '') => async () => {
  const filter = { skip, order }
  const result = await axios.get(`${BASE}?filter=${JSON.stringify(filter)}`)

  return result.data
}

export const getCount = () => async () => {
  const result = await axios.get(`${BASE}/count`)

  return result.data
}
