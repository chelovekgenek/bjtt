import axios from 'helpers/axios'
import {
  STORAGE_IMAGES
} from 'constants/index'

const BASE = 'storages'

export const uploadImage = form => async () => {
  const result = await axios.post(`${BASE}/${STORAGE_IMAGES}/upload`, form)

  return result.data
}
