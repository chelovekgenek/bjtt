import { TOKEN } from 'constants/index'

export function clear() {
  localStorage.removeItem(TOKEN)
  sessionStorage.removeItem(TOKEN)
}
export function get() {
  const token = localStorage.getItem(TOKEN) || sessionStorage.getItem(TOKEN)

  return token
}
export function set(token, remember = true) {
  if (remember === true) {
    localStorage.setItem(TOKEN, token)
  } else {
    sessionStorage.setItem(TOKEN, token)
  }
}
export function isRemember() {
  if (localStorage.getItem(TOKEN)) {
    return true
  }
  return false
}
