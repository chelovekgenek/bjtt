const { getApp } = require('../helpers/');

module.exports = async function(User) {
  const app = await getApp(User);

  User.me = (req, cb) => {
    cb(null, req.accessToken);
  };
  User.remoteMethod('me', {
    accepts: { arg: 'req', type: 'object', http: { source: 'req' } },
    returns: { type: 'object', root: true },
    http: { path: '/me', verb: 'get' },
    description: 'find AccessToken object by provided token',
  });
};
