import { call, put, takeEvery, select } from 'redux-saga/effects'
import { browserHistory } from 'react-router'

import * as c from 'constants/actions'
import * as a from 'actions/task'
import * as r from 'constants/routes'

import * as api from './api/task'
import * as selector from './selectors/task'
import * as apiStorage from './api/storage'

function* uploadImage(image) {
  const form = new FormData()
  form.append('file', image, 'image.png')
  const file = yield call(apiStorage.uploadImage(form))
  return file.result.files.file[0].name
}
function* handleAdd({ payload }) {
  try {
    const data = payload
    if (data.image !== '') {
      data.image = yield uploadImage(data.image)
    }
    const result = yield call(api.add(data))
    yield put(a.successAdd(result))
    browserHistory.push(r.TASK_LIST)
  } catch ({ message }) {
    yield put(a.errorAdd(message))
  }
}
function* handleEdit({ payload }) {
  try {
    const data = payload.data
    const selected = yield select(selector.getSelected)
    if (data.image !== '' && data.image !== selected.image) {
      data.image = yield uploadImage(data.image)
    }
    const result = yield call(api.edit(payload.id, data))
    yield put(a.successEdit(result))
    browserHistory.push(r.TASK_LIST)
  } catch (e) {
    yield put(a.errorEdit())
  }
}
function* handleGetList({ payload }) {
  try {
    const data = yield call(api.getList(payload.skip, payload.order))
    const { count } = yield call(api.getCount())
    yield put(a.successGetList({ data, count, skip: payload.skip }))
  } catch ({ message }) {
    yield put(a.errorGetList(message))
  }
}
function handleSelect() {
  browserHistory.push(r.TASK_EDIT)
}

export default function* watcherTask() {
  yield takeEvery(c.API_ADD_TASK_REQUEST, handleAdd)
  yield takeEvery(c.API_EDIT_TASK_REQUEST, handleEdit)
  yield takeEvery(c.API_GET_TASK_LIST_REQUEST, handleGetList)
  yield takeEvery(c.SELECT_TASK, handleSelect)
}
