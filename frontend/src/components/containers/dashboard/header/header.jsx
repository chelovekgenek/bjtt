import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import { Button } from 'react-bootstrap'

import { requestLogout as logout } from 'actions/user'
import { LOGIN } from 'constants/routes'

import styles from './header.module.scss'

const Header = props => (
  <div className={styles.container}>
    { props.user.authenticated === false ? (
      <Button
        className={styles.btn}
        onClick={() => browserHistory.push(LOGIN)}
      >Log In</Button>
    ) : (
      <div className={styles.content}>
        <div className={styles.title}>
          <h4>{props.user.data.username}</h4>
          <p>{props.user.data.email}</p>
        </div>
        <Button
          className={styles.btn}
          onClick={() => props.logout()}
        >Log Out</Button>
      </div>
    )}
  </div>
)

Header.propTypes = {
  user: PropTypes.shape({
    authenticated: PropTypes.bool.isRequired,
    data: PropTypes.shape({
      username: PropTypes.string,
      email: PropTypes.string
    }).isRequired
  }).isRequired,

  logout: PropTypes.func.isRequired
}

export default connect(
  state => ({
    user: state.rootReducer.user
  }),
  dispatch => ({
    logout: bindActionCreators(logout, dispatch)
  })
)(Header)
