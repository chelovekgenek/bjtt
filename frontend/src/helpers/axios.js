import axios from 'axios'

import { get as getToken } from 'helpers/token'

const PRM_TOKEN = 'access_token'

export default axios.create({
  baseURL: Config.backend.url,
  timeout: 3000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json'
  },
  params: {
    [PRM_TOKEN]: null
  },
  validateStatus: status => (status >= 200 && status < 300) || (status === 500),
  paramsSerializer: (params) => {
    const serialized = Object.entries(params).map((item) => {
      const param = item
      if (param[0] === PRM_TOKEN) {
        const token = getToken()

        if (token === null) return ''

        param[1] = token
      }
      return param.join('=')
    })
    return serialized.join('&')
  }
})
