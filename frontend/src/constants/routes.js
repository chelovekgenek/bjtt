export const INDEX = '/'
export const ANY = '*'

export const LOGIN = '/login'

export const TASK_LIST = '/tasks'
export const TASK_NEW = `${TASK_LIST}/new`
export const TASK_EDIT = `${TASK_LIST}/edit`
