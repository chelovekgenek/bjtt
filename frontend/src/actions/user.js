import * as c from 'constants/actions'

export function requestLogin(data) {
  return {
    type: c.API_LOGIN_USER_REQUEST,
    payload: data
  }
}
export function successLogin(data) {
  return {
    type: c.API_LOGIN_USER_SUCCESS,
    payload: data
  }
}
export function errorLogin(error) {
  return {
    type: c.API_LOGIN_USER_ERROR,
    payload: error
  }
}

export function requestLogout(data) {
  return {
    type: c.API_LOGOUT_USER_REQUEST,
    payload: data
  }
}
export function successLogout(data) {
  return {
    type: c.API_LOGOUT_USER_SUCCESS,
    payload: data
  }
}
export function errorLogout(error) {
  return {
    type: c.API_LOGOUT_USER_ERROR,
    payload: error
  }
}
