import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import {
  Button, ButtonToolbar, Modal, Checkbox
} from 'react-bootstrap'
import ImageTools from 'resize-image'
import ImageToBlob from 'image-to-blob'

import { isEmailValid, getImageUrl } from 'helpers/'
import { TaskCard, FormInput } from 'components/commons/'
import {
  requestAdd as addTask,
  requestEdit as editTask,
  deselect as deselectFromEdit
} from 'actions/task'
import { TASK_LIST } from 'constants/routes'

import styles from './task-new.module.scss'

class TaskNew extends React.PureComponent {
  constructor(props) {
    super(props)

    this.handleEmail = e => this.setState({
      email: e.target.value,
      errorEmail: !isEmailValid(e.target.value)
    })
    this.handleUsername = e => this.setState({
      username: e.target.value,
      errorUsername: e.target.value.length === 0
    })
    this.handleText = e => this.setState({
      text: e.target.value,
      errorText: e.target.value.length === 0
    })
    this.handleImage = ::this.handleImage
    this.handleSubmit = ::this.handleSubmit
    this.handleShowPreview = ::this.handleShowPreview
    this.handleClosePreview = () => this.setState({ modalOpen: false })
    this.handleDone = () => this.setState({ done: !this.state.done })
    this.handleClearImage = () => this.setState({ image: '' })
    this.handleInitialImage = () => this.setState({ image: getImageUrl(props.selected.image) })
    this.gotoTaskList = () => browserHistory.push(TASK_LIST)

    this.state = {
      email: props.edit === true ? props.selected.email : '',
      username: props.edit === true ? props.selected.username : '',
      text: props.edit === true ? props.selected.text : '',
      image: getImageUrl(props.selected.image),
      done: props.edit === true ? props.selected.done : false,
      modalOpen: false,
      errorEmail: false,
      errorUsername: false,
      errorText: false,
      errorImage: false
    }
  }
  componentWillUnmount() {
    if (this.props.edit === true) {
      this.props.deselectFromEdit()
    }
  }
  render() {
    return (
      <div className={`${styles.container} content-wrapper`}>
        <form>
          <FormInput
            label='Email'
            value={this.state.email}
            error={this.state.errorEmail}
            onChange={this.handleEmail}
          />
          <FormInput
            label='Username'
            value={this.state.username}
            error={this.state.errorUsername}
            onChange={this.handleUsername}
          />
          <FormInput
            label='Text'
            componentClass='textarea'
            rows='5'
            className={styles.text}
            value={this.state.text}
            error={this.state.errorText}
            onChange={this.handleText}
          />
          <div className={styles.image}>
            <FormInput
              label='Image'
              type='file'
              accept='image/jpeg,image/png'
              onChange={this.handleImage}
            />
            <ButtonToolbar className={styles.image_control}>
              {this.state.image !== '' && (
                <Button onClick={this.handleClearImage}>
                  Clear
                </Button>
              )}
              {this.props.edit === true && this.state.image !== this.props.selected.image && (
                <Button onClick={this.handleInitialImage}>
                  Initial
                </Button>
              )}
            </ButtonToolbar>
          </div>
          {this.state.image !== '' && <img id='img-preview' src={this.state.image} />}
        </form>
        {this.props.edit === true && (
          <Checkbox
            checked={this.state.done}
            onChange={this.handleDone}
          >Done</Checkbox>
        )}
        <ButtonToolbar>
          <Button
            bsStyle='info'
            onClick={this.handleShowPreview}
          >Preview</Button>
          <Button
            bsStyle='danger'
            onClick={this.gotoTaskList}
          >Cancel</Button>
          <Button
            bsStyle='success'
            onClick={this.handleSubmit}
          >Save</Button>
        </ButtonToolbar>
        <Modal
          bsSize='lg'
          show={this.state.modalOpen}
          onHide={this.handleClosePreview}
        >
          <Modal.Body>
            { this.state.modalOpen === true && (
              <TaskCard
                preview
                username={this.state.username}
                email={this.state.email}
                text={this.state.text}
                image={this.state.image}
              />
            )}
          </Modal.Body>
        </Modal>
      </div>
    )
  }
  handleImage(e) {
    const fr = new FileReader()
    fr.onload = () => {
      const img = new Image()
      img.onload = () => {
        const maxHeight = 240
        const resize = (width = maxHeight, height = maxHeight) => this.setState({
          image: ImageTools.resize(img, width, height, ImageTools.PNG)
        })
        const fromWidth = img.width
        const fromHeight = img.height
        const pDiff = (Math.abs(fromWidth - fromHeight) / ((fromWidth + fromHeight) / 2)) * 100
        const diffHeight = (maxHeight * pDiff) / 100
        if (fromWidth > fromHeight) {
          return resize(maxHeight + diffHeight)
        }
        if (fromWidth < fromHeight) {
          return resize(maxHeight - diffHeight)
        }
        return resize()
      }
      img.src = fr.result
    }
    fr.readAsDataURL(e.target.files[0])
  }
  handleSubmit() {
    const { email, username, text, image, done } = this.state
    const dispatch = (blob = '') => {
      if (this.isStateValid() !== true) return
      if (this.props.edit === true) {
        this.props.editTask(this.props.selected.id, { email, username, text, done, image: blob })
      } else {
        this.props.addTask({ email, username, text, image: blob })
      }
    }
    if (image !== '') {
      ImageToBlob(
        document.getElementById('img-preview'),
        (err, blob) => dispatch(blob)
      )
    } else {
      dispatch()
    }
  }
  handleShowPreview() {
    if (this.isStateValid() !== true) return

    this.setState({ modalOpen: true })
  }
  isStateValid() {
    const { errorEmail, errorUsername, errorText } = this.state
    if (errorEmail === true
    || errorUsername === true
    || errorText === true) {
      return false
    }
    return true
  }
}
TaskNew.defaultProps = {
  edit: false
}
TaskNew.propTypes = {
  edit: PropTypes.bool,
  addTask: PropTypes.func.isRequired,
  editTask: PropTypes.func.isRequired,
  selected: PropTypes.shape({
    id: PropTypes.string,
    email: PropTypes.string,
    username: PropTypes.string,
    text: PropTypes.string,
    image: PropTypes.string,
    done: PropTypes.bool
  }).isRequired,
  deselectFromEdit: PropTypes.func.isRequired
}

export default connect(
  state => ({
    selected: state.rootReducer.task.selected
  }),
  dispatch => ({
    addTask: bindActionCreators(addTask, dispatch),
    editTask: bindActionCreators(editTask, dispatch),
    deselectFromEdit: bindActionCreators(deselectFromEdit, dispatch)
  })
)(TaskNew)
