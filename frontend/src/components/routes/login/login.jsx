import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Panel, Button } from 'react-bootstrap'

import { FormInput } from 'components/commons'
import { requestLogin as login } from 'actions/user'

import styles from './login.module.scss'

class Login extends React.PureComponent {
  constructor(props) {
    super(props)

    this.handleUsername = e => this.setState({
      username: e.target.value,
      errorUsername: e.target.value === ''
    })
    this.handlePassword = e => this.setState({
      password: e.target.value,
      errorPassword: e.target.value === ''
    })
    this.handleSubmit = ::this.handleSubmit

    this.state = {
      username: 'admin',
      password: '123',
      errorUsername: false,
      errorPassword: false
    }
  }
  render() {
    return (
      <div className={styles.container}>
        <Panel >
          <h2>Login</h2>
          <br />
          <form>
            <FormInput
              label='Username'
              value={this.state.username}
              error={this.state.errorUsername}
              onChange={this.handleUsername}
            />
            <FormInput
              label='Password'
              type='password'
              value={this.state.password}
              error={this.state.errorPassword}
              onChange={this.handlePassword}
            />
          </form>
          <br />
          <Button
            bsStyle='primary'
            block
            disabled={this.props.fetching}
            onClick={this.handleSubmit}
          >Submit</Button>
        </Panel>
      </div>
    )
  }
  handleSubmit() {
    const { username, password } = this.state

    if (username.length === 0 || password.length === 0) {
      return false
    }

    return this.props.login({ username, password })
  }
}
Login.propTypes = {
  fetching: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired
}

export default connect(
  state => ({
    fetching: state.rootReducer.user.fetching
  }),
  dispatch => ({
    login: bindActionCreators(login, dispatch)
  })
)(Login)
