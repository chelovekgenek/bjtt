import { ENV_PRODUCTION, STORAGE_IMAGES } from 'constants/index'

export function setupEnvironment() {
  window.isProduction = (process.env.NODE_ENV === ENV_PRODUCTION)
}

export function isEmailValid(string) {
  // eslint-disable-next-line
  return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(string)
}

export function getImageUrl(image) {
  if (typeof image !== 'string' || image === '') {
    return ''
  }
  return `${Config.backend.url}storages/${STORAGE_IMAGES}/download/${image}`
}
