import { TaskNew } from 'components/routes'

const TaskEdit = () => (
  <TaskNew edit />
)

export default TaskEdit
