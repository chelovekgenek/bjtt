import { fork, all } from 'redux-saga/effects'

import watcherTask from './task'
import watcherUser from './user'

export default function* rootSaga() {
  yield all([
    fork(watcherTask),
    fork(watcherUser)
  ])
}
