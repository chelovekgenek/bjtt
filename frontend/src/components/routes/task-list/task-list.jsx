import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  Button, Pagination, FormGroup, FormControl, ControlLabel
} from 'react-bootstrap'
import { browserHistory } from 'react-router'
import Spinner from 'react-spinkit'

import { TASK_RECORD_LIMIT } from 'constants/index'
import { TASK_NEW } from 'constants/routes'
import { TaskCard } from 'components/commons/'
import { getImageUrl } from 'helpers/'
import {
  requestGetList as getTasks,
  select as selectTaskToEdit
} from 'actions/task'

import styles from './task-list.module.scss'

const SORT_FIELDS = [{
  label: '-',
  value: ''
}, {
  label: 'Email',
  value: 'email'
}, {
  label: 'Username',
  value: 'username'
}, {
  label: 'Status',
  value: 'done'
}]
const SORT_ORDER = [{
  label: 'Ascending',
  value: 'ASC'
}, {
  label: 'Descending',
  value: 'DESC'
}]

class TaskList extends React.PureComponent {
  static propTypes = {
    getTasks: PropTypes.func.isRequired,
    selectTaskToEdit: PropTypes.func.isRequired,
    tasks: PropTypes.shape({
      data: PropTypes.array.isRequired,
      fetching: PropTypes.bool.isRequired,
      count: PropTypes.number.isRequired,
      skip: PropTypes.number.isRequired
    }).isRequired,
    authenticated: PropTypes.bool.isRequired
  }
  constructor(props) {
    super(props)

    const handleSort = (prop, value) => {
      this.setState({ [prop]: value }, () => {
        const { field, order } = this.state
        if (field !== '') {
          this.props.getTasks({ order: `${field} ${order}` })
        }
      })
    }
    this.gotoTaskNew = () => browserHistory.push(TASK_NEW)
    this.handlePageSelected = page => this.props.getTasks({ skip: (page - 1) * TASK_RECORD_LIMIT })
    this.handleSortField = e => handleSort('field', e.target.value)
    this.handleSortOrder = e => handleSort('order', e.target.value)

    this.state = {
      field: '',
      order: 'ASC'
    }
  }
  componentDidMount() {
    this.props.getTasks()
  }
  render() {
    let content = null
    const { tasks } = this.props
    switch (true) {
      case tasks.fetching === true:
        content = (
          <div className={styles.spinner}>
            <Spinner name='ball-spin-fade-loader' />
          </div>
        )
        break
      case tasks.data.length === 0:
        content = (
          <h3>No data found</h3>
        )
        break
      default:
        content = tasks.data.map(item => (
          <TaskCard
            key={item.id}
            username={item.username}
            email={item.email}
            text={item.text}
            done={item.done}
            image={getImageUrl(item.image)}
            onEdit={this.getCardEdit(item)}
          />
        ))
    }
    return (
      <div className={`${styles.container} content-wrapper`}>
        <div className={styles.panel}>
          <FormGroup>
            <ControlLabel>Sort by:</ControlLabel>
            <FormControl
              componentClass='select'
              value={this.state.field}
              onChange={this.handleSortField}
            >
              { SORT_FIELDS.map(item => (
                <option key={item.value} value={item.value}>{item.label}</option>
              )) }
            </FormControl>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Order: </ControlLabel>
            <FormControl
              componentClass='select'
              value={this.state.order}
              onChange={this.handleSortOrder}
            >
              { SORT_ORDER.map(item => (
                <option key={item.value} value={item.value}>{ item.label }</option>
              )) }
            </FormControl>
          </FormGroup>
        </div>
        <Button
          className={styles.add}
          onClick={this.gotoTaskNew}
        >
          Add New
        </Button>
        { content }
        {(tasks.count > TASK_RECORD_LIMIT) && (<Pagination
          bsSize='large'
          items={Math.ceil(tasks.count / TASK_RECORD_LIMIT)}
          activePage={Math.floor(tasks.skip / TASK_RECORD_LIMIT) + 1}
          onSelect={this.handlePageSelected}
        />)}
        <br />
      </div>
    )
  }
  getCardEdit(item) {
    if (this.props.authenticated === true) {
      return () => this.props.selectTaskToEdit(item)
    }
    return null
  }
}

export default connect(
  state => ({
    tasks: state.rootReducer.task,
    authenticated: state.rootReducer.user.authenticated
  }),
  dispatch => ({
    getTasks: bindActionCreators(getTasks, dispatch),
    selectTaskToEdit: bindActionCreators(selectTaskToEdit, dispatch)
  })
)(TaskList)
