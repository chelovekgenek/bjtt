import * as c from 'constants/actions'

export function select(payload) {
  return {
    type: c.SELECT_TASK,
    payload
  }
}
export function deselect() {
  return {
    type: c.DESELECT_TASK
  }
}

export function requestAdd(payload) {
  return {
    type: c.API_ADD_TASK_REQUEST,
    payload
  }
}
export function successAdd(payload) {
  return {
    type: c.API_ADD_TASK_SUCCESS,
    payload
  }
}
export function errorAdd() {
  return {
    type: c.API_ADD_TASK_ERROR
  }
}

export function requestEdit(id, data) {
  return {
    type: c.API_EDIT_TASK_REQUEST,
    payload: {
      id, data
    }
  }
}
export function successEdit(payload) {
  return {
    type: c.API_EDIT_TASK_SUCCESS,
    payload
  }
}
export function errorEdit() {
  return {
    type: c.API_EDIT_TASK_ERROR
  }
}

export function requestGetList(payload = {}) {
  return {
    type: c.API_GET_TASK_LIST_REQUEST,
    payload: {
      skip: isNaN(payload.skip) ? 0 : payload.skip,
      ...payload
    }
  }
}
export function successGetList(payload) {
  return {
    type: c.API_GET_TASK_LIST_SUCCESS,
    payload
  }
}
export function errorGetList() {
  return {
    type: c.API_GET_TASK_LIST_ERROR
  }
}
