import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Router, Route, Redirect, IndexRedirect } from 'react-router'

import { App, Dashboard } from 'components/containers'
import { Login, TaskList, TaskNew, TaskEdit } from 'components/routes'
import { requestLogin } from 'actions/user'

import * as r from 'constants/routes'

class Routes extends React.Component {
  constructor(props) {
    super(props)

    this.initialAuth = ::this.initialAuth
  }
  shouldComponentUpdate() {
    return false
  }
  render() {
    return (
      <Router history={this.props.history}>
        <Route path={r.INDEX} component={App} >
          <IndexRedirect to={r.TASK_LIST} />
          <Route path={r.LOGIN} component={Login} />
          <Route component={Dashboard} onEnter={this.initialAuth}>
            <Route path={r.TASK_LIST} component={TaskList} />
            <Route path={r.TASK_NEW} component={TaskNew} />
            <Route path={r.TASK_EDIT} component={TaskEdit} />
          </Route>
          <Redirect from={r.ANY} to={r.INDEX} />
        </Route>
      </Router>
    )
  }
  initialAuth() {
    if (this.props.authenticated === false) {
      this.props.requestLogin({ byToken: true })
    }
  }
}

Routes.propTypes = {
  authenticated: PropTypes.bool.isRequired,

  history: PropTypes.object.isRequired,
  requestLogin: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    authenticated: state.rootReducer.user.authenticated
  }
}
function mapDispatchToProps(dispatch) {
  return {
    requestLogin: bindActionCreators(requestLogin, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Routes)
