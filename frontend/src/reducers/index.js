import { combineReducers } from 'redux'

import task from './task'
import user from './user'

const rootReducer = combineReducers({
  task, user
  // ...your other reducers here
})

export default rootReducer
